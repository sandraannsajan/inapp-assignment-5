import sqlite3
conn = sqlite3.connect('emp.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")
query1 = """CREATE TABLE EMPLOYEE(
        NAME CHAR(20) NOT NULL,
        ID INT NOT NULL,
        SALARY INT NOT NULL,
        DEPT_ID INT NOT NULL)"""
cursor.execute(query1)
cursor.execute("ALTER TABLE EMPLOYEE ADD CITY CHAR(20)")
query2 = "INSERT INTO EMPLOYEE(NAME, ID, SALARY, DEPT_ID, CITY) VALUES (?,?,?,?,?)"
for i in range(5):
    print(i+1)
    name=input("Enter Name: ")
    id=int(input("Enter ID: "))
    sal=int(input("Enter salary: "))
    dept=int(input("Enter dept ID: "))
    city=input("Enter city: ")
    print("\n")
    cursor.execute(query2,(name,id,sal,dept,city))

output=cursor.execute("SELECT NAME, ID, SALARY from EMPLOYEE")
print("DETAILS")
print(output.fetchall())

ch=input("\nEnter starting letter of name that you want details of:")
output1=cursor.execute("SELECT NAME FROM EMPLOYEE WHERE NAME LIKE '{}%' ".format(ch.upper()))
print("\n NAMES OF EMPLOYEES STARTING WITH {}".format(ch.upper()))
print(output1.fetchall())

a=int(input("\n Enter ID to be checked:"))
output2=cursor.execute("SELECT * FROM EMPLOYEE WHERE ID=={} ".format(a))
print(output2.fetchall())

a=int(input("\n Enter ID who's name is to be changed:"))
n=input("Enter new name:")
cursor.execute("UPDATE EMPLOYEE SET NAME='{}' WHERE ID=={}".format(n,a))
output3=cursor.execute("SELECT * FROM EMPLOYEE WHERE ID=={} ".format(a))
print("\nUpdated name and details")
print(output3.fetchall())

cursor.execute("DROP TABLE IF EXISTS DEPARTMENT")
query3 = """CREATE TABLE DEPARTMENT(
        DEPT_ID CHAR(20) NOT NULL,
        DEPT_NAME CHAR(20) NOT NULL)"""
cursor.execute(query3)

query4 = "INSERT INTO DEPARTMENT(DEPT_ID, DEPT_NAME) VALUES (?,?)"
for i in range(5):
    print("\n")
    print(i+1)
    id=input("Enter dept ID: ")
    name=input("Enter dept name: ")
    cursor.execute(query4, (id,name))

d=int(input("\nEnter dept ID to get details of employees in that dept"))
output4=cursor.execute("SELECT DEPT_NAME FROM DEPARTMENT WHERE DEPT_ID=={}".format(d))
print("\nThis is the department: {}".format(output4.fetchall()))

output5=cursor.execute("SELECT * from EMPLOYEE where DEPT_ID=={}".format(d))
print("\nEmployee details from the department")
print(output5.fetchall())

conn.commit()
conn.close()




